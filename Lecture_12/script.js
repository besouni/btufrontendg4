function key_press(e){
    console.log("Press");
    console.log(e);
    console.log("Key -> "+e.key);
    console.log("keyCode -> "+e.keyCode);
    console.log("charCode -> "+e.charCode);
    console.log("--------------")
}

function key_down(e){
    console.log("Down");
    console.log(e);
    console.log("Key -> "+e.key);
    console.log("keyCode -> "+e.keyCode);
    console.log("charCode -> "+e.charCode);
    console.log("--------------")
}

document.getElementById("info").addEventListener("keydown", function (e){
   key_down(e);
})

function key_up(e){
    console.log("Up");
    console.log(e);
    console.log("Key -> "+e.key);
    console.log("keyCode -> "+e.keyCode);
    console.log("charCode -> "+e.charCode);
    console.log("--------------")
}