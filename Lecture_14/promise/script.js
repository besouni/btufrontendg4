async function f() {
    let promise = new Promise((resolve, reject) => {
        setTimeout(() => resolve("done!"), 1000)
    });
    let result = await promise; // wait until the promise resolves (*)
    alert(result); // "done!"
}

f();


// async function f() {
//     return 13;
// }
//
// f().then(function (value){
//     console.log(value);
// }); // 1


// function myDisplayer(some) {
//     document.getElementById("demo").innerHTML = some;
// }

// new Promise(function(resolve, reject) {
//
//     setTimeout(function () {resolve(1)}, 1000); // (*)
//
// }).then(function(result) { // (**)
//
//     alert(result); // 1
//     return result * 2;
//
// }).then(function(result) { // (***)
//
//     alert(result); // 2
//     return result * 2;
//
// }).then(function(result) {
//
//     alert(result); // 4
//     return result - 7;
//
// }).then(function(result) {
//
//     alert(result); // 4
//     return result * 2;
//
// });

// let promise = new Promise(function(resolve, reject) {
//     // the function is executed automatically when the promise is constructed
//
//     // after 1 second signal that the job is done with the result "done"
//     setTimeout(() => resolve("done"), 1000);
//     // setTimeout(function() {resolve("done")}, 1000);
// });

// let myPromise = new Promise(function(myResolve, myReject) {
//     let x = 0;
//
// // The producing code (this may take some time)
//
//     if (x == 0) {
//         myResolve("OK");
//         // myResolve("skjdhbdshkjjdk");
//
//     } else {
//         myReject("Error");
//     }
// });
//
// myPromise.then(
//     function(value, value1="dsds") {
//         myDisplayer(value + " Myresolve"+" "+value1);
//     },
//     function(error) {myDisplayer(error + "MyRject");}
// );

// function myDisplayer(some) {
//     document.getElementById("demo").innerHTML = some;
// }
//
// function clickF(){
//     myCalculator(5, 5, myDisplayer);
// }
//
// function myCalculator(num1, num2, myCallback) {
//     let sum = num1 + num2;
//     myCallback(sum);
// }
//
// myCalculator(51, 51, myDisplayer);





// setTimeout(function (){
//     console.log("Call Back");
// }, 2000)
//
// setTimeout(f2, 2000)
//
// function f2(){
//     console.log("F2")
// }
//
